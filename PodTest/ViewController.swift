//
//  ViewController.swift
//  PodTest
//
//  Created by Vasil Nunev on 11/05/2017.
//  Copyright © 2017 nunev. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        let fView = FantasticView(frame: self.view.bounds)
        
        self.view.addSubview(fView)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

