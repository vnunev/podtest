//
//  FantasticView.swift
//  PodTest
//
//  Created by Vasil Nunev on 11/05/2017.
//  Copyright © 2017 nunev. All rights reserved.
//

import UIKit

class FantasticView: UIView {
    
    let colours : [UIColor] = [.red, .purple, .blue, .black, .gray, .orange]
    var counter = 0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        let scheduledColorChanged = Timer.scheduledTimer(withTimeInterval: 2.0, repeats: true) { (_) in
            UIView.animate(withDuration: 2.0, animations: { 
                self.layer.backgroundColor =  self.colours[self.counter % 6].cgColor
                self.counter += 1
            })
        }
        scheduledColorChanged.fire()
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
